# SISURAT Fakultas Farmasi UI

## Tentang

![Preview SISURAT](photos/SI SURAT.png)

SISURAT adalah sistem informasi persuratan berbasis web Fakultas Farmasi Universitas Indonesia. Sistem ini dibuat untuk meningkatkan kemudahan, efektivitas, dan efisiensi proses persuratan. Terdapat 3 peran pada sistem ini, yaitu Mahasiswa, Admin, dan Eksekutif.

Sistem ini mengakomodasi proses permintaan surat yang terstruktur yang dimulai dari Mahasiswa yang memilih jenis surat, mengisi formulir, lalu membuat permintaan surat. Kemudian, Admin dapat meneruskan permintaan tersebut ke Eksekutif yang bersangkutan. Jika disetujui, maka Admin dapat mengunggah surat ke sistem untuk selanjutnya diunduh oleh Mahasiswa. Admin dan Eksekutif juga dapat menolak permintaan surat dan memasukkan alasan penolakannya. Sistem ini akan mengirimkan notifikasi berupa email untuk setiap jenis aktivitas yang dilakukan.

Selain itu, Admin juga dapat menambah jenis surat beserta formulirnya dan melakukan manajemen pengguna yaitu mengubah peran. Admin dan Mahasiswa melihat statistik ketepatan waktu penyelesaian surat sesuai dengan Pedoman Operasional Baku (POB).

## Demo
https://sisurat-farmasi-ui.herokuapp.com/  
Anda butuh akun SSO Fakultas Farmasi UI untuk mengakses demo website ini

## Credit

Kelompok Proyek Pengembangan Sistem Informasi C03 - SISURAT  
Fakultas Ilmu Komputer Universitas Indonesia  
Klien: Fakultas Farmasi Universitas Indonesia  
Dosen: Dr. Panca Oktavia Hadi Putra B.Sc., M.Bus.  
Asisten dosen: Karin Patricia  

Anggota:

- Najla Laila Muharram - Project Manager - 1906399612
- Cut Zahra Nabila - Lead Analyst - 1906399934
- Ahmad Harori Zaki Ichsan - Lead Programmer - 1906353965
- Reno Fathoni - Scrum Master - 1906399461
- Fareeha Nisa Zayda Azeeza - Lead Designer - 1906399644
