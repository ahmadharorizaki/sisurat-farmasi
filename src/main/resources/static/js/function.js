var ul = document.getElementsByTagName("UL");

for(var i=0, j=ul.length; i<j; i++) {
    if ( /\bbuttonGroup\b/.test(ul[i].className) ) {
        ul[i].onclick = segmentedController(ul[i]);
    }
}

function segmentedController(target) {
    const base_url = "https://sisurat-farmasi-ui.herokuapp.com"
    return function(event) {
        var li = target.getElementsByTagName("LI");
        for (var i=0, j=li.length; i<j; i++) {
            var _ = li[i];
            _.className = _.className.replace(/\bselected\b/, "")
            document.getElementById('name').value = "";


            if (_ === event.target) {
                _.className += " selected";
                var Data = {
                    "Id" : i
                }
                $.ajax({
                    type: "POST",
                    contentType : 'application/json; charset=utf-8',
                    dataType : 'json',
                    data: JSON.stringify(Data),//send data directly
                    url: base_url + "/permintaanSurat",
                    asynch: true,
                    success : function (data) {
                        var str = ""
                        $('table tbody').html('')
                        $('table tbody').html('')
                        $('#none').html('')
                        if (data.result.length == 0) {
                            none = '<br>' + '<br>' + '<h5 style="text-align:center; margin-left: 50 px"> Tidak terdapat permintaan surat dengan status ini.</h5>';
                            $('#none').append(none);
                            $('#kosong').remove()
                        } else {
                            $('#kosong').remove()
                            for (var i = 0; i < data.result.length; i++) {
                                var number = i+1;
                                str = '<tr>' +  '<td>' + number + '</td>' +
                                '<td>' + data.result[i].jenisSurat + '</td>' +
                                    '<td>' + data.result[i].nama + '</td>' +
                                    '<td>' + data.result[i].waktuDiajukan + '</td>' +
                                    '<td>' + data.result[i].bahasaSurat + '</td>' +
                                    '<td>' + data.result[i].bentukSurat + '</td>' +
                                    '<td>' + data.result[i].status + '</td>' +
                                    '<td>' +
                                    '<div className="align-items-center">' +
                                    '<a href="/detailSurat/' + data.result[i].idPermintaanSurat + '" class="btn btn-primary">Lihat Detail</a>' +
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                                $('table tbody').append(str);
                            }
                        }
                    }
                });
            }
        }


    }
}