package ui.farmasi.sisurat.pdf;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import ui.farmasi.sisurat.model.UserModel;

import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTML;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PDFExporter {
    
    private static final Map<String, String> BULAN_MAP = new HashMap<>();

    public PDFExporter(){
        BULAN_MAP.put("01", "Januari");
        BULAN_MAP.put("02", "Februari");
        BULAN_MAP.put("03", "Maret");
        BULAN_MAP.put("04", "April");
        BULAN_MAP.put("05", "Mei");
        BULAN_MAP.put("06", "Juni");
        BULAN_MAP.put("07", "Juli");
        BULAN_MAP.put("08", "Agustus");
        BULAN_MAP.put("09", "September");
        BULAN_MAP.put("10", "Oktober");
        BULAN_MAP.put("11", "November");
        BULAN_MAP.put("12", "Desember");
    }


    public void suratKeteranganAktif(HttpServletResponse response,
                                     String nama,
                                     String npm,
                                     String semester){

        Document document = new Document(PageSize.A4, 50,50,50,50);
        try{
            PdfWriter.getInstance(document, response.getOutputStream());

            document.open();

            String content ="Yang bertanda tangan di bawah ini :\n" +
                    "\n" +
                    "nama: Delly Ramadon, M.Farm., Ph.D., Apt\n" +
                    "NUP: 100111610261400991\n" +
                    "jabatan: Ketua Program Studi S1 \n" +
                    "\n" +
                    "menerangkan dengan sebenarnya bahwa :  \n" +
                    "\n" +
                    "nama\t\t: %s\n" +
                    "NPM\t\t: %s\n" +
                    "\n" +
                    "adalah benar sebagai mahasiswa dengan status akademis aktif pada Program Studi S1 Farmasi, Fakultas Farmasi Universitas Indonesia semester %s\n" +
                    "\n" +
                    "Demikian surat keterangan ini dibuat untuk dapat digunakan sebagaimana mestinya. \n\n\n";

            String dtf = DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now());

            String dateToday = dtf.substring(8) + " " +
                    BULAN_MAP.get(dtf.substring(5,7)) + " " +
                    dtf.substring(0,4);

            String sign = dateToday + "\n" +
                    "Ketua Program Studi S1 FF UI\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Delly Ramadon, M.Farm., Ph.D., Apt\n" +
                    "NUP 100111610261400991";

            Paragraph title = new Paragraph("SURAT KETERANGAN\n",
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 14));
            title.setAlignment(Element.ALIGN_CENTER);

            Phrase subtitle = new Phrase("Nomor : KET-   /UN2.F15.P1/PDP.01.05.02/2022\n\n",
                    FontFactory.getFont( FontFactory.TIMES_ROMAN, 12));
            title.add(subtitle);

            Paragraph contents = new Paragraph(String.format(content, nama, npm, semester),
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 12));

            Paragraph signs = new Paragraph(sign,
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 12));
            signs.setIndentationLeft(250);

            document.add(title);
            document.add(contents);
            document.add(signs);

        } catch (DocumentException de){
            System.err.println(de.getMessage());
        } catch (IOException ioe){
            System.err.println(ioe.getMessage());
        }

        document.close();
    }

    public void suratPermohonanKajiEtik(HttpServletResponse response,
                                        String tujuanSurat,
                                        String nama,
                                        String npm,
                                        String noHp,
                                        String judulPenelitian){
        Document document = new Document(PageSize.A4, 50,50,50,50);

        try{
            PdfWriter.getInstance(document, response.getOutputStream());

            document.open();

            String content =
                    "Yth.   : %s\n" +
                    "Dari   : Wakil Dekan Bidang Bidang Pendidikan, Penelitian dan Kemahasiswaan FF UI\n" +
                    "Perihal: Permohonan Pengajuan Ethical Approval \n" +
                    "\n" +
                    "Bersama ini dengan hormat kami mohon bantuan agar Komite Kaji Etik Penelitian institusi Bapak/Ibu dapat memberikan keterangan lolos kaji etik (Ethical Approval) untuk protokol penelitian sebagai berikut : \n" +
                    "\n" +
                    "Nama            : %s \n" +
                    "Npm             : %s \n" +
                    "No hp           : %s \n" +
                    "Judul Penelitian: %s \n" +
                    "\n" +
                    "\n" +
                    "Terlampir kami sampaikan :\n" +
                    "a. Formulir Etik Penelitian Kesehatan 1 kopi harus diisi dengan lengkap dan jelas\n" +
                    "b. Protokol Penelitian 1 kopi\n" +
                    "c. Formulir Informed Consent 1 kopi\n" +
                    "d. Susunan Tim peneliti dan / CV Biodata peneliti Utama\n" +
                    "e. Persetujuan kepala institusi yang berwenang\n" +
                    "f. Bukti transfer dana kaji etik\n" +
                    "g. Pengisian data secara online melalui http://research.fk.ui.ac.id/ethics/\n" +
                    "\n" +
                    "Demikian surat permohonan ini kami sampaikan. Atas perhatian Bapak/Ibu, kami ucapkan terima kasih.\n\n\n";

            String dtf = DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now());

            String dateToday = dtf.substring(8) + " " +
                    BULAN_MAP.get(dtf.substring(5,7)) + " " +
                    dtf.substring(0,4);

            String sign =  dateToday + "\n" +
                    "Wakil Dekan Bidang Pendidikan, Penelitian dan Kemahasiswaan,\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Dr. Fadlina Chany Saputri, M.Si., Apt\n" +
                    "NIP 197804192008122001\n";

            Paragraph title = new Paragraph("NOTA DINAS\n",
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 14));
            title.setAlignment(Element.ALIGN_CENTER);

            Phrase subtitle = new Phrase("Nomor: ND-  /UN2.F15.D1/PDP.04.04/2022\n\n",
                    FontFactory.getFont( FontFactory.TIMES_ROMAN, 12));
            title.add(subtitle);

            Paragraph contents = new Paragraph(String.format(content, tujuanSurat, nama, npm,noHp, judulPenelitian),
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 12));

            Paragraph signs = new Paragraph(sign,
                    FontFactory.getFont(FontFactory.TIMES_ROMAN, 12));
            signs.setIndentationLeft(250);

            document.add(title);
            document.add(contents);
            document.add(signs);

        } catch (DocumentException de){
            System.err.println(de.getMessage());
        } catch (IOException ioe){
            System.err.println(ioe.getMessage());
        }

        document.close();
    }
}
