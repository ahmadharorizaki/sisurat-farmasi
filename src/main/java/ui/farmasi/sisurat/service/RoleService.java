package ui.farmasi.sisurat.service;

import ui.farmasi.sisurat.model.RoleModel;

import java.util.Optional;

public interface RoleService {
    RoleModel getByName(String name);
    RoleModel getById(Integer id);
}
