package ui.farmasi.sisurat.service;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import ui.farmasi.sisurat.model.RiwayatModel;

import java.util.List;

public interface RiwayatService {
    void addRiwayat(RiwayatModel riwayat);
    RiwayatModel getRiwayatByIdSurat(PermintaanSuratModel permintaanSurat);
}
