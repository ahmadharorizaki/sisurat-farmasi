package ui.farmasi.sisurat.service;

import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.List;

@Service
public interface PermintaanSuratService {
    List<PermintaanSuratModel> getListPermintaanSurat();
    List<PermintaanSuratModel> getListPermintaanSuratMahasiswa(MahasiswaModel mhs);
    void addSurat(PermintaanSuratModel surat);
    void updateSurat(PermintaanSuratModel surat);
    void deleteSurat(PermintaanSuratModel surat);
    PermintaanSuratModel getPermintaanSuratById(Long id);
    List<PermintaanSuratModel> getListPermintaanByStatus(Byte status);
    List<PermintaanSuratModel> getListPermintaanByProdi(String prodi);
    void deleteAllByMahasiswa(MahasiswaModel mahasiswa);
}
