package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.repository.MahasiswaDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class MahasiswaServiceImpl implements MahasiswaService{

    @Autowired
    private MahasiswaDb mahasiswaDb;

    @Override
    public MahasiswaModel getMahasiswabyID(Long id) {
        return mahasiswaDb.findMahasiswaModelById(id);
    }

    public List<MahasiswaModel> getMahasiswaByNama (String nama) {
        return mahasiswaDb.findMahasiswaModelByNameContainsIgnoreCase(nama);
    }

    @Override
    public  MahasiswaModel getMahasiswaByNPM (String npm) {
        return mahasiswaDb.findMahasiswaModelBynpm(npm);
    }

    @Override
    public void delete(MahasiswaModel mahasiswa) {
        mahasiswaDb.delete(mahasiswa);
        mahasiswaDb.flush();
    }

}