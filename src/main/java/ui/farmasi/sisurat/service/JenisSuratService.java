package ui.farmasi.sisurat.service;

import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;

import java.util.List;

public interface JenisSuratService {

    JenisSuratModel saveJenisSurat(JenisSuratModel jenisSurat);
    JenisSuratModel updateJenisSurat(JenisSuratModel jenisSurat);
    List<JenisSuratModel> getListJenisSurat();
    JenisSuratModel getSuratById(Integer id);
    JenisSuratModel deleteJenisSurat (JenisSuratModel jenis);
    List<JenisSuratModel> getListJenisSuratByProdi(String prodi);
}

