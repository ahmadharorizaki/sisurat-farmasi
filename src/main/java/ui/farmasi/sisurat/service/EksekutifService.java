package ui.farmasi.sisurat.service;

import ui.farmasi.sisurat.model.EksekutifModel;

import java.util.List;

public interface EksekutifService {
    List<EksekutifModel> getAllEksekutif();
    EksekutifModel getEksekutifById(Long id);
    void delete(EksekutifModel eksekutif);
}
