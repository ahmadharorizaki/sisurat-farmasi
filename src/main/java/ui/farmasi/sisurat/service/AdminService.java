package ui.farmasi.sisurat.service;

import ui.farmasi.sisurat.model.AdminModel;

import java.util.List;
import java.util.Optional;

public interface AdminService {
    List<AdminModel> getAllAdmin();
    List<AdminModel> getAllAdminByProdi(String prodi);
    void delete(AdminModel adminModel);
    AdminModel getByUsername(String username);
}
