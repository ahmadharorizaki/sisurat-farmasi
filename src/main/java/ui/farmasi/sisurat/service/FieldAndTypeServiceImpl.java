package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.repository.FieldAndTypeDb;

import javax.transaction.Transactional;

@Service
@Transactional
public class FieldAndTypeServiceImpl implements FieldAndTypeService{

    @Autowired
    FieldAndTypeDb fieldAndTypeDb;

    @Override
    public void deleteById(Long id) {
        fieldAndTypeDb.deleteById(id);
    }
}
