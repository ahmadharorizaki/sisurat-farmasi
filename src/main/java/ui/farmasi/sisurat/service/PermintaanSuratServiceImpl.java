package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.model.RiwayatModel;
import ui.farmasi.sisurat.repository.PermintaanSuratDb;

import javax.transaction.Transactional;
import java.util.List;

import java.util.Optional;

@Service
@Transactional
public class PermintaanSuratServiceImpl implements PermintaanSuratService{
    @Autowired
    private PermintaanSuratDb permintaanSuratDb;

    @Override
    public List<PermintaanSuratModel> getListPermintaanSurat(){
        return permintaanSuratDb.findAll();
    }

    @Override
    public void updateSurat (PermintaanSuratModel surat){
       permintaanSuratDb.saveAndFlush(surat);
    }

    //Method untuk mendelete surat
    @Override
    public void deleteSurat(PermintaanSuratModel surat) {
        permintaanSuratDb.delete(surat);
        permintaanSuratDb.flush();
    }

    @Override
    public List<PermintaanSuratModel> getListPermintaanSuratMahasiswa(MahasiswaModel mhs){
        return permintaanSuratDb.findAllByMahasiswa(mhs);
    }

    @Override
    public void addSurat(PermintaanSuratModel surat) {
        permintaanSuratDb.save(surat);
    }


    @Override
    public PermintaanSuratModel getPermintaanSuratById(Long id) {
        Optional<PermintaanSuratModel> surat = permintaanSuratDb.findByIdPermintaanSurat(id);
        if (surat.isPresent()) {
            return surat.get();
        }
        return null;
    }

    @Override
    public List<PermintaanSuratModel> getListPermintaanByStatus(Byte status){
        return permintaanSuratDb.findAllByStatus(status);
    }

    @Override
    public List<PermintaanSuratModel> getListPermintaanByProdi(String prodi){
        return permintaanSuratDb.findAllByProdiMahasiswa(prodi);
    }

    @Override
    public void deleteAllByMahasiswa(MahasiswaModel mahasiswa) {
        permintaanSuratDb.deleteAllByMahasiswa(mahasiswa);
        permintaanSuratDb.flush();
    }

}

