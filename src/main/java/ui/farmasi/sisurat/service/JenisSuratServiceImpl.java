package ui.farmasi.sisurat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ui.farmasi.sisurat.model.JenisSuratModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.repository.JenisSuratDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class JenisSuratServiceImpl implements JenisSuratService{
    @Autowired
    JenisSuratDb jenisSuratDb;

    @Override
    public JenisSuratModel saveJenisSurat(JenisSuratModel jenisSurat) {
        return jenisSuratDb.save(jenisSurat);
    }

    @Override
    public JenisSuratModel updateJenisSurat(JenisSuratModel jenisSurat) {
        return jenisSuratDb.save(jenisSurat);
    }

    @Override
    public List<JenisSuratModel> getListJenisSurat() {
        return jenisSuratDb.findAll();
    }

    @Override
    public JenisSuratModel getSuratById(Integer id) {
        Optional<JenisSuratModel> surat = jenisSuratDb.findById(id);
        return surat.orElse(null);
    }

    @Override
    public JenisSuratModel deleteJenisSurat (JenisSuratModel jenis){
            jenisSuratDb.delete(jenis);
            return jenis;
    }

    @Override
    public List<JenisSuratModel> getListJenisSuratByProdi(String prodi) {
        return jenisSuratDb.findAllByProdi(prodi);
    }


}
