package ui.farmasi.sisurat.form;

import ui.farmasi.sisurat.model.JenisSuratModel;

import java.util.List;

public class SuratForm {

    private JenisSuratModel jenisSurat;

    private Integer bahasaSurat;

    private Integer bentukSurat;

    private String keperluan;

    private List<FieldForm> textFields;

    private List<List<ListFieldForm>> listFields;

    private Integer listCount;

    public JenisSuratModel getJenisSurat() {
        return jenisSurat;
    }

    public void setJenisSurat(JenisSuratModel jenisSurat) {
        this.jenisSurat = jenisSurat;
    }

    public Integer getBahasaSurat() {
        return bahasaSurat;
    }

    public void setBahasaSurat(Integer bahasaSurat) {
        this.bahasaSurat = bahasaSurat;
    }

    public List<FieldForm> getTextFields() {
        return textFields;
    }

    public void setTextFields(List<FieldForm> textFields) {
        this.textFields = textFields;
    }

    public List<List<ListFieldForm>> getListFields() {
        return listFields;
    }

    public void setListFields(List<List<ListFieldForm>> listFields) {
        this.listFields = listFields;
    }

    public Integer getBentukSurat() {
        return bentukSurat;
    }

    public void setBentukSurat(Integer bentukSurat) {
        this.bentukSurat = bentukSurat;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
    }

    public Integer getListCount() {
        return listCount;
    }

    public void setListCount(Integer listCount) {
        this.listCount = listCount;
    }

}
