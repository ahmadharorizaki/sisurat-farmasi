package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.model.RiwayatModel;

import java.util.List;

@Repository
public interface RiwayatDb extends JpaRepository<RiwayatModel, Long> {

    RiwayatModel findByPermintaanSurat(PermintaanSuratModel permintaanSurat);
}
