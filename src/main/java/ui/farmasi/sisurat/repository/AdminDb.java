package ui.farmasi.sisurat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ui.farmasi.sisurat.model.AdminModel;

import java.util.List;
import java.util.Optional;

public interface AdminDb extends JpaRepository<AdminModel, Long> {
    List<AdminModel> findAll();
    List<AdminModel> findAllByProdi(String prodi);
    AdminModel findByUsername(String username);
}
