package ui.farmasi.sisurat.model;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;


@Entity
@Table(name = "field_list")
public class FieldListModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String value;

    private Integer listId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn (referencedColumnName = "idPermintaanSurat")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PermintaanSuratModel permintaanSurat;

    public FieldListModel(){
    }

    public FieldListModel(String name, String value, Integer listId, PermintaanSuratModel permintaanSurat) {
        this.name = name;
        this.value = value;
        this.listId = listId;
        this.permintaanSurat = permintaanSurat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }

    public PermintaanSuratModel getPermintaanSurat() {
        return permintaanSurat;
    }

    public void setPermintaanSurat(PermintaanSuratModel permintaanSurat) {
        this.permintaanSurat = permintaanSurat;
    }
}
