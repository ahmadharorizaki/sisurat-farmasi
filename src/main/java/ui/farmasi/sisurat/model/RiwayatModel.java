package ui.farmasi.sisurat.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "riwayat")
public class RiwayatModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String pengubah;

    @NotNull
    @Column(nullable = false)
    private Byte status;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime tanggalWaktu;

    @ManyToOne
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PermintaanSuratModel permintaanSurat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPengubah() {
        return pengubah;
    }

    public void setPengubah(String pengubah) {
        this.pengubah = pengubah;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public LocalDateTime getTanggalWaktu() {
        return tanggalWaktu;
    }

    public void setTanggalWaktu(LocalDateTime tanggalWaktu) {
        this.tanggalWaktu = tanggalWaktu;
    }

    public PermintaanSuratModel getPermintaanSurat() {
        return permintaanSurat;
    }

    public void setPermintaanSurat(PermintaanSuratModel permintaanSurat) {
        this.permintaanSurat = permintaanSurat;
    }
}
