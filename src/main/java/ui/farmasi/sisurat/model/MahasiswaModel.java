package ui.farmasi.sisurat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ui.farmasi.sisurat.service.RoleService;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@Setter
@Getter
@Entity
public class MahasiswaModel extends UserModel {

    private String npm;

    @OneToMany(mappedBy = "mahasiswa", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<PermintaanSuratModel> listPermintaanSurat;

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public List<PermintaanSuratModel> getListPermintaanSurat() {
        return listPermintaanSurat;
    }

    public void setListPermintaanSurat(List<PermintaanSuratModel> listPermintaanSurat) {
        this.listPermintaanSurat = listPermintaanSurat;
    }
}
