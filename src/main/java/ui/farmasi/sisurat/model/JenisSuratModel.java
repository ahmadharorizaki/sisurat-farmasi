package ui.farmasi.sisurat.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "jenis_surat")
public class JenisSuratModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(nullable = false)
    private String nama;

    private String prodi;

    @OneToMany(mappedBy = "jenisSurat", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<FieldAndTypeModel> fieldsAndTypes;

    public JenisSuratModel(){
        fieldsAndTypes = new ArrayList<>();
    }

    private Integer batasWaktu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getBatasWaktu() {
        return batasWaktu;
    }

    public void setBatasWaktu(Integer batasWaktu) {
        this.batasWaktu = batasWaktu;
    }

    public List<FieldAndTypeModel> getFieldsAndTypes() {
        return fieldsAndTypes;
    }

    public void setFieldsAndTypes(List<FieldAndTypeModel> fieldsAndTypes) {
        this.fieldsAndTypes = fieldsAndTypes;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }
}
