package ui.farmasi.sisurat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.File;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "permintaan_surat")
public class PermintaanSuratModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPermintaanSurat;

    private Byte status;

    private Byte bentukSurat;

    private Integer bahasaSurat;

    private String alasanPenolakan;

    private String keperluan;

    @NotNull
    @Column(nullable = false)
    private String prodiMahasiswa;

    private Integer durasi;

    private LocalDate tanggalPembuatan;

    private LocalDate tanggalSelesai;

    @OneToMany(mappedBy = "permintaanSurat", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<FieldModel> fields;

    @OneToMany(mappedBy = "permintaanSurat", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<FieldListModel> fieldlists;

    @OneToMany(mappedBy = "permintaanSurat", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<RiwayatModel> riwayat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    private JenisSuratModel jenisSurat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MahasiswaModel mahasiswa;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @OnDelete(action = OnDeleteAction.CASCADE)
    private FileModel file;

    @ManyToMany
    @JoinTable(
            name = "permintaansurat_eksekutif",
            joinColumns = @JoinColumn(name = "id_permintaan_surat"),
            inverseJoinColumns = @JoinColumn(name = "id_eksekutif")
    )
    private List<EksekutifModel> listEksekutif;

    public void setDurasi(Integer durasi) {
        this.durasi = durasi;
    }


    public LocalDate getTanggalPembuatan() {
        return tanggalPembuatan;
    }

    public void setTanggalPembuatan(LocalDate tanggalPembuatan) {
        this.tanggalPembuatan = tanggalPembuatan;
    }

    public LocalDate getTanggalSelesai() {
        return tanggalSelesai;
    }

    public void setTanggalSelesai(LocalDate tanggalSelesai) {
        this.tanggalSelesai = tanggalSelesai;
    }

    public Long getIdPermintaanSurat() {
        return idPermintaanSurat;
    }

    public void setIdPermintaanSurat(Long idPermintaanSurat) {
        this.idPermintaanSurat = idPermintaanSurat;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getBentukSurat() {
        return bentukSurat;
    }

    public void setBentukSurat(Byte bentukSurat) {
        this.bentukSurat = bentukSurat;
    }

    public Integer getBahasaSurat() {
        return bahasaSurat;
    }

    public void setBahasaSurat(Integer bahasaSurat) {
        this.bahasaSurat = bahasaSurat;
    }

    public String getAlasanPenolakan() {
        return alasanPenolakan;
    }

    public void setAlasanPenolakan(String alasanPenolakan) {
        this.alasanPenolakan = alasanPenolakan;
    }

    public List<FieldModel> getFields() {
        return fields;
    }

    public void setFields(List<FieldModel> fields) {
        this.fields = fields;
    }

    public List<RiwayatModel> getRiwayat() {
        return riwayat;
    }

    public void setRiwayat(List<RiwayatModel> riwayat) {
        this.riwayat = riwayat;
    }

    public JenisSuratModel getJenisSurat() {
        return jenisSurat;
    }

    public void setJenisSurat(JenisSuratModel jenisSurat) {
        this.jenisSurat = jenisSurat;
    }

    public MahasiswaModel getMahasiswa() {
        return mahasiswa;
    }

    public Integer getDurasi(){ return this.durasi;}

    public void setMahasiswa(MahasiswaModel mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public List<EksekutifModel> getListEksekutif() {
        return listEksekutif;
    }

    public void setListEksekutif(List<EksekutifModel> listEksekutif) {
        this.listEksekutif = listEksekutif;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
    }

    public List<FieldListModel> getFieldlists() {
        return fieldlists;
    }

    public void setFieldlists(List<FieldListModel> fieldlists) {
        this.fieldlists = fieldlists;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }
    
    public String getProdiMahasiswa() { return prodiMahasiswa; }

    public void setProdiMahasiswa(String prodiMahasiswa) { this.prodiMahasiswa = prodiMahasiswa;
    }
}
