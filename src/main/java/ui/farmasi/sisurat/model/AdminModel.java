package ui.farmasi.sisurat.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@AllArgsConstructor
@Setter
@Getter
@Entity
public class AdminModel extends UserModel {
}
