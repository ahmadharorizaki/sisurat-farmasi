package ui.farmasi.sisurat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.ModelAndView;
import ui.farmasi.sisurat.model.AdminModel;
import ui.farmasi.sisurat.model.EksekutifModel;
import ui.farmasi.sisurat.model.MahasiswaModel;
import ui.farmasi.sisurat.model.UserModel;
import ui.farmasi.sisurat.rest.AuthenticationSuccess;
import ui.farmasi.sisurat.rest.ServiceResponse;
import ui.farmasi.sisurat.setting.Setting;
import ui.farmasi.sisurat.service.RoleService;
import ui.farmasi.sisurat.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;


@Controller
public class BaseController {

    private static final String SPRING_SECURITY_CONTEXT_KEY = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
    private final WebClient webClient = WebClient.builder().build();

    @Autowired
    private RoleService roleService;

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    @GetMapping(value = "/dashboard")
    public String homeAdmin() {
        return "statistik";
    }

    @GetMapping(value = "/validate-ticket")
    public ModelAndView login(
            @RequestParam(value = "ticket", required = false) String ticket,
            HttpServletRequest request
    ) {
        List<String> kode_org = Arrays. asList("01.00.17.01", "02.00.17.01", "03.00.17.01",
                "04.00.17.01", "05.00.17.01", "06.00.17.01");

        List <String> specialUser = Arrays.asList("najla.laila", "ahmad.harori",
                "fareeha.nisa", "reno.fathoni", "cut.zahra91", "rivany.triany",
                "karin.patricia", "adelya.gabriel");

        List <String> admin = Arrays.asList("maulida.fs", "tini21", "arni81",
                "lies.w", "rian.padhila");

        List <String> eksekutif = Arrays.asList("arry.yanuar", "fadlina", "r.iswandana",
                "ramadondelly17", "catur.jatmika", "h.suryadi");

        ServiceResponse srp =  this.webClient.get().uri(
                String.format(
                        Setting.SERVER_VALIDATE_TICKET,
                        ticket,
                        Setting.CLIENT_LOGIN
                )
        ).retrieve().bodyToMono(ServiceResponse.class).block();
        AuthenticationSuccess user = srp.getAuthenticationSuccess();

        if (!kode_org.contains(user.getAttributes().getKd_org())){
            if (!specialUser.contains(user.getUser())) {
                return new ModelAndView("redirect:/forbidden");
            }
        }
        // User belum terdaftar di sistem
        if (userService.getByUsername(user.getUser()) == null) {
            if (admin.contains(user.getUser())){
                AdminModel newUser = new AdminModel();
                newUser.setRole(roleService.getByName("ADMIN"));
                setAttributes(newUser, user);
            } else if (eksekutif.contains(user.getUser())){
                EksekutifModel newUser = new EksekutifModel();
                newUser.setRole(roleService.getByName("EKSEKUTIF"));
                setAttributes(newUser, user);
            } else {
                MahasiswaModel newUser = new MahasiswaModel();
                newUser.setRole(roleService.getByName("MAHASISWA"));
                newUser.setNpm(user.getAttributes().getNpm());
                setAttributes(newUser, user);
            }
        }

        Authentication auth = new UsernamePasswordAuthenticationToken(user.getUser(), "SISURAT");

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        HttpSession session = request.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);

        return new ModelAndView("redirect:/");
    }

    @GetMapping("/")
    public ModelAndView mainMenu(
            HttpServletRequest request
    ) {
        if (request.isUserInRole("ROLE_ADMIN")) {
            return new ModelAndView("redirect:/statistik");
        }
        return new ModelAndView("redirect:/daftarSurat");
    }

    @GetMapping(value = "/direct-to-sso")
    public ModelAndView directToSSO(){
        return new ModelAndView("redirect:"+ Setting.SERVER_LOGIN + Setting.CLIENT_LOGIN);
    }

    @GetMapping(value = "/login-to-web")
    public String loginPage() {
        return "login-to-sso";
    }

    @GetMapping(value = "/forbidden")
    public String forbidden() {
        return "forbidden";
    }

    @GetMapping(value = "/logouts")
    public ModelAndView logout() {
        return new ModelAndView("redirect:" + Setting.SERVER_LOGOUT + Setting.CLIENT_LOGOUT);
    }

    public void setAttributes (UserModel newUser, AuthenticationSuccess user){
        newUser.setEmailUI(user.getUser() + "@ui.ac.id");
        newUser.setUsername(user.getUser());
        newUser.setPassword("{noop}SISURAT");
        newUser.setName(user.getAttributes().getNama());
        String prodi = user.getAttributes().getKd_org();

        //Admin Fakultas
        List <String> adminFakultas = Arrays.asList( "tini21");

        if (adminFakultas.contains(user.getUser())) {
            newUser.setProdi("ADMIN FAKULTAS");
        }
        else if (prodi.equals("01.00.17.01") || prodi.equals("02.00.17.01")){
            newUser.setProdi("S1");
        } else if (prodi.equals("04.00.17.01") || prodi.equals("05.00.17.01")) {
            newUser.setProdi("S2");
        } else if (prodi.equals("03.00.17.01")){
            newUser.setProdi("Apoteker");
        }
        else if (prodi.equals("06.00.17.01")){
            newUser.setProdi("S3");
        } else {
            newUser.setProdi("SPECIAL USER");
        }
        userService.saveUser(newUser);
    }

}
