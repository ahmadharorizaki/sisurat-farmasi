package ui.farmasi.sisurat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ui.farmasi.sisurat.model.FieldModel;
import ui.farmasi.sisurat.model.PermintaanSuratModel;
import ui.farmasi.sisurat.pdf.PDFExporter;
import ui.farmasi.sisurat.service.PermintaanSuratService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class PdfController {

    @Qualifier("permintaanSuratServiceImpl")
    @Autowired
    private PermintaanSuratService permintaanSuratService;


    @GetMapping("/pdf/{idSurat}")
    public void exportToPdf(
            HttpServletResponse response,
            @PathVariable Long idSurat
    ) throws IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        PDFExporter exporter = new PDFExporter();

        PermintaanSuratModel permintaan = permintaanSuratService.getPermintaanSuratById(idSurat);
        String namaMhs = permintaan.getMahasiswa().getName();
        String npmMhs = permintaan.getMahasiswa().getNpm();
        String semester = null;
        for (FieldModel field : permintaan.getFields()) {
            if (field.getName().equals("Semester")) {
                semester = field.getValue();
            }
        }

        exporter.suratKeteranganAktif(response, namaMhs, npmMhs, semester);
    }


}
